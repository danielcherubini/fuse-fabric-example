using Uno;
using Uno.Compiler.ExportTargetInterop;
using Fuse.Scripting;
using Uno.Threading;

public extern (!Android && !iOS) class FabricDigits {
	public FabricDigits() {
	}
  public void Login(Promise<string> promise) {
    promise.Reject(new Exception("NOPE!!!!!!!!"));
	}
}

[Require("Xcode.ShellScript", "\\\"${PODS_ROOT}/Fabric/run\\\" @(Project.Mobile.Fabric.APIKey) @(Project.Mobile.Fabric.BuildSecret)")]
[Require("Source.Include", "Fabric/Fabric.h")]
[Require("Source.Include", "DigitsKit/DigitsKit.h")]
[Require("Source.Include", "@{Exception:Include}")]
public extern(iOS) class FabricDigits {

  [Foreign(Language.ObjC)]
  public FabricDigits()
  @{
    [Fabric with:@[[Digits class]]];
	@}

  [Foreign(Language.ObjC)]
  public void Login(Promise<string> promise)
	@{
    Digits *digits = [Digits sharedInstance];

    [digits logOut];
    [digits authenticateWithCompletion:^(DGTSession *session, NSError *error) {
      // Inspect session/error objects
      if (session) {
        //success
        DGTOAuthSigning *oauthSigning = [[DGTOAuthSigning alloc] initWithAuthConfig:digits.authConfig authSession:digits.session];
        NSDictionary *authHeaders = [oauthSigning OAuthEchoHeadersToVerifyCredentials];

        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:authHeaders
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        //return data here
        @{Promise<string>:Of(promise).Resolve(string):Call(jsonString)};
      } else {
        //write error
        @{Promise<string>:Of(promise).Reject(Exception):Call(@{Exception(string):New(@"Sorry not sorry")})};
      }
    }];
	@}
}

[Require("Gradle.Repository", "maven { url 'https://maven.fabric.io/public' }")]
[Require("Gradle.Dependency.Compile", "com.digits.sdk.android:digits:1.10.3")]
[Require("Gradle.Dependency.Compile", "io.fabric.sdk.android:fabric:1.3.10")]

[ForeignInclude(Language.Java, "com.digits.sdk.android.Digits", "com.twitter.sdk.android.core.TwitterAuthConfig", "com.twitter.sdk.android.core.TwitterCore", "io.fabric.sdk.android.Fabric", "com.digits.sdk.android.AuthCallback", "com.digits.sdk.android.DigitsAuthConfig", "com.digits.sdk.android.DigitsException", "com.digits.sdk.android.DigitsSession" ,"org.json.JSONObject", "java.util.Map" ,"com.digits.sdk.android.DigitsOAuthSigning", "com.twitter.sdk.android.core.TwitterAuthToken")]
public extern(Android) class FabricDigits {

	[Foreign(Language.Java)]
	public FabricDigits()
	@{

		#if @(Project.Mobile.Fabric.ConsumerKey:IsSet)
			final String TWITTER_KEY = "@(Project.Mobile.Fabric.ConsumerKey)";
			final String TWITTER_SECRET = "@(Project.Mobile.Fabric.ConsumerSecret)";
		#endif

		TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
		Fabric.with(com.fuse.Activity.getRootActivity(), new TwitterCore(authConfig), new Digits());
	@}

	[Foreign(Language.Java)]
	public void Login(Promise<string> promise)
	@{

		Digits.getSessionManager().clearActiveSession();

		AuthCallback authCallback = new AuthCallback() {
			@Override
			public void success(DigitsSession session, String phoneNumber) {

				TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
				TwitterAuthToken authToken = (TwitterAuthToken) session.getAuthToken();
				DigitsOAuthSigning oauthSigning = new DigitsOAuthSigning(authConfig, authToken);
				Map<String, String> authHeaders = oauthSigning.getOAuthEchoHeadersForVerifyCredentials();

				final String jsonString = new JSONObject(authHeaders).toString();

				//return data here
				@{Promise<string>:Of(promise).Resolve(string):Call(jsonString)};

			}
			@Override
			public void failure(DigitsException exception) {
				//write error
				@{Promise<string>:Of(promise).Reject(Exception):Call(@{Exception(string):New("Sorry not sorry")})};
			}
    };

    DigitsAuthConfig.Builder digitsAuthConfigBuilder = new DigitsAuthConfig.Builder()
            .withAuthCallBack(authCallback)
            .withThemeResId(com.fuse.Activity.getRootActivity().getResources().getIdentifier("CustomDigitsTheme", "style", com.fuse.Activity.getRootActivity().getPackageName()));

    Digits.authenticate(digitsAuthConfigBuilder.build());
	@}
}
