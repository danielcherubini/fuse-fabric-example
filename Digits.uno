using Fuse;
using Fuse.Scripting;
using Fuse.Reactive;
using Uno.Threading;
using Uno.UX;

[UXGlobalModule]
public class Digits : NativeModule
{
	static readonly Digits _instance;
	static FabricDigits _digits;

	public Digits()
	{
		if(_instance != null) return;

    Resource.SetGlobalKey(_instance = this, "Digits");
		AddMember(new NativePromise<string, string>("login", Login, null));
		AddMember(new NativeFunction("init", Init));
	}

	public static object Init(Context c, object[] args) {
		if(_digits == null) _digits = new FabricDigits();
		debug_log("digits init");

		// return null;
	}

	public static Future<string> Login(object[] args)
	{
		debug_log("logging in");
		var promise = new Promise<string>(Fuse.UpdateManager.Dispatcher);

		_digits.Login(promise);

		return promise;
	}
}
